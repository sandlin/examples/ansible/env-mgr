import subprocess
import re
import json
import sys
from pprint import pprint

REF_LINK="https://docs.ansible.com/ansible-lint/rules/default_rules.html#default-rules"

'''
This script executes ansible-lint & generates the CodeQuality report in json format required by GitLab.
https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool
'''

class Issue():

    def __init__(self, filepath, linenum, description):
        self.type = "issue"
        self.location = {}
        self.location['path'] = filepath
        self.location['lines'] = {}
        self.location['lines']['begin'] = linenum
        self.description = description

def main(argv):
    p = subprocess.Popen('ansible-lint', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    issues = []
    for line in p.stdout.readlines():
        l = line.decode('utf-8')
        m = re.match(r'([^\:]*)\:(\d+)\:\s\[(E\d+)\]\s\[(\w+)\]\s(.*)$', l)
        try:
            filename = m.group(1)
            linenum = m.group(2)
            rule = m.group(3)
            severity = m.group(4)
            description = m.group(5)
            longdesc = "{} - {} - {}".format(severity, rule, description)
            i = Issue(filename, linenum, longdesc)
            issues.append(i)
        except AttributeError as e:
            print(e)
    json_string = json.dumps([ob.__dict__ for ob in issues])
    print(json_string)
    with open("gl-code-quality-report.json", 'w') as f:
        f.write(json_string)
    f.close()
    retval = p.wait()

if __name__ == "__main__":
    print("main")
    sys.exit(main(sys.argv[1:]))
