FROM registry.gitlab.com/sandlin/dockerfiles/ansible-ci:latest

USER root

WORKDIR /opt/devops/workspaces

CMD ["ping", "-q", "-i", "60", "127.0.0.1"]