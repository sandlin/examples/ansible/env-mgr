# env-mgr

## Overview
This Ansible role / play is being used to demonstrate environment specific configuration management & generation.

- The command to be called will be `ansible-playbook generate.yml`. 
- In order to specify the app/env to generate, we'll use [Ansible's Pattern functionality](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html).
  - Limit which config to generate via Ansible's `--limit` or `-l` functionality. 
  - Specify the `app name` && `env level` in the following format: `<app>:\&<env>`
  - examples:
    - `-l app2:\&test`
    - `-l app2:\&prod`
    - `-l app1:\&stage`

### Project Layout
#### group_vars
- Each environment will be defined here.
#### inventory
- This is where we define our apps (`app1`, `app2`) & the map of which environment is used for each env level (`test`, `stage`, `demo`, `prod`)
#### roles
- This is where the config generator logic & template live.
#### target
- Where the generated config will reside.
#### generate.yml
- The Ansible Playbook being executed


### Run Locally
1. Start container
   ```docker-compose up -d```
2. Enter container
   ```docker exec -it envmgrdemo /bin/sh```
3. Execute Ansible Calls
   - `ansible-playbook generate.yml -l app2:\&test`
   - `ansible-playbook generate.yml -l app2:\&stage`
   - `ansible-playbook generate.yml -l app1:\&test`
   - `ansible-playbook generate.yml -l app1:\&prod`
4. View your file in the `target` directory
   ```more target/appsettings.json```

